<?php 
    require("./processing/common.php");
    
    if(!isset($_SESSION['login_user'])){
     header("location: ./loginpage.php");
    }
    
    
    
    
    $cameraid = $_GET['cameraid'];
    $videolist = getvideolist($db, $cameraid);
    $videolistcount = count($videolist);
    
    
    function getvideolist($db, $cameraid){
        $videolistquery = "SELECT `videodate`, `videoid` FROM `video` WHERE camera_id = ".$cameraid." ORDER BY videodate";
        $videolistq = mysqli_query($db, $videolistquery);
        $videolist = mysqli_fetch_all($videolistq);
        return $videolist;
    }
    
    ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Project Akita</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile support -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Material Design fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap Material Design -->
        <link href="dist/css/bootstrap-material-design.css" rel="stylesheet">
        <link href="dist/css/ripples.min.css" rel="stylesheet">
        <!-- Dropdown.js -->
        <link href="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">
        <!-- Page style -->
        <link href="index.css" rel="stylesheet">
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <div class="bs-component">
            <div class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0)">Project Akita</a>
                    </div>
                    <div class="navbar-collapse collapse navbar-inverse-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="./index.html">Home</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">User Panel</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Support</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="./processing/logout.php">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- THIS IS WHERE THE THING STARTS AFTER THE NAVBAR-->
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" action="videodisplay.php"  method="post">
                            <fieldset>
                                <legend>View archive footage by date</legend>
                                <div class="form-group">
                                    <label for="select111" class="col-md-2 control-label">Select date</label>
                                    <div class="col-md-10">
                                        <select name="videoid" class="form-control">



                                        <?php 
                                            for($x=0; $x<$videolistcount; $x++){
                                              echo '<option value="'.$videolist[$x][1].'">'.$videolist[$x][0].'</option>';
                                            }
                                            ?>




                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-2">
                                        <button type="text" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <h2>Some video data or video statistics for the video here. </h2>
            </div>
        </div>
    </body>
</html>