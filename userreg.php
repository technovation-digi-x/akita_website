<?php session_start(); ?>

<!DOCTYPE html>

<html>

<head>
  <title>PROJEC AKITA</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Mobile support -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Material Design fonts -->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <!-- Bootstrap Material Design -->
  <link href="dist/css/bootstrap-material-design.css" rel="stylesheet">
  <link href="dist/css/ripples.min.css" rel="stylesheet">

  <!-- Dropdown.js -->
  <link href="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">

  <!-- Page style -->
  <link href="index.css" rel="stylesheet">

  <!-- jQuery -->
  <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

</head>
<body>

<div class="bs-component">
          <div class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="javascript:void(0)">Project Akita</a>
              </div>
              <div class="navbar-collapse collapse navbar-inverse-collapse">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="./index.html">Home</a></li>
                    <li><a href="javascript:void(0)">User Panel</a></li>
                  <li><a href="javascript:void(0)">Support</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="javascript:void(0)">Sign in</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="userreg.php">Register</a></li>
                </ul>                
              </div>
            </div>
          </div>
</div>
    
    <div class="im-centered">
    
<form class="form-horizontal" method="post" autocomplete="off" action="./processing/reg.php">
  <fieldset>
      <div class="row"><div class="col-md-1"></div><div class="col-md-10"><legend>ACCOUNT REGRISTRATION</legend></div></div>
      <div class="row"><div class="col-md-1"></div><div class="col-md-10"><?php if(isset($_SESSION['errmsg'])){echo $_SESSION['errmsg']; }?></div></div>
    
      <div class="row">
          <div class="col-md-10">
          <div class="form-group">
      <label for="email" class="col-md-2 control-label">Email</label>

      <div class="col-md-10">
        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword" class="col-md-2 control-label">Password</label>

      <div class="col-md-10">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
      </div>
    </div>
      
          <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
          <input type="submit" name="submit" class="btn btn-raised btn-success">
      </div>
    </div>
      
      </div>
      <div class="col-md-1"></div>
      </div>

    








  </fieldset>
        </form></div>
        
        
        
</body>
</html>



