<?php session_start(); ?>

<html>

<head>
  <title>PROJEC AKITA</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Mobile support -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Material Design fonts -->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">

  <!-- Bootstrap Material Design -->
  <link href="../dist/css/bootstrap-material-design.css" rel="stylesheet">
  <link href="../dist/css/ripples.min.css" rel="stylesheet">

  <!-- Dropdown.js -->
  <link href="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">

  <!-- Page style -->
  <link href="../index.css" rel="stylesheet">

  <!-- jQuery -->
  <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

</head>
<body>

<div class="bs-component">
          <div class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="javascript:void(0)">Project Akita</a>
              </div>
              <div class="navbar-collapse collapse navbar-inverse-collapse">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="./index.html">Home</a></li>
                    <li><a href="../controlpanel.php">User Panel</a></li>
                  <li><a href="javascript:void(0)">Support</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="javascript:void(0)">Sign in</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="userreg.html">Register</a></li>
                </ul>                
              </div>
            </div>
          </div>
</div>
    
    <div class="row"><div class="col-md-2"></div><div class="col-md-10"><legend>Please insert the following into your camera settings</legend></div></div>
    <div class="row"><div class="col-md-2"></div><div class="col-md-10"><legend>Host Name/Server Name: ftp://akita.alicerobotics.ml | Port: 21 | Passive Mode</legend></div></div>
    <div class="row"><div class="col-md-2"></div><div class="col-md-10"><legend>Your FTP username: <?php echo $_SESSION['ftpuser'] ?></legend></div></div>   
    <div class="row"><div class="col-md-2"></div><div class="col-md-10"><legend>Your FTP password: <?php echo $_SESSION['ftppass'] ?></legend></div></div>   
    <div class="row"><div class="col-md-2"></div><div class="col-md-6"><legend><b>FTP home directory: /<?php echo $_SESSION['cameraid'] ?>/</b></legend></div></div>  
    
    <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-6">
        <img src="../images/DlinkSetupFtpService.jpg" class="img-rounded" alt="DlinkSetup">
         </div>
    <div class="col-md-5">
        <h1>SETUP GUIDE</h1>
        <h5>
        1. Navigate to your camera's IP address in your web browser.<br>Example: http://192.168.1.150<br><br>
        2. Go to FTP server settings.<br><br>
        3. Insert your given FTP username, FTP password and path into the fields.<br><br>
        4. Tick the checkbox for time schedule. Select always for periodic uploads.<br><br>
        5. Enter your allowed image frequency that your subscription allows.<br><br>Entering an incorrect number may get your account blocked.<br><br>
        6. Select Date/Time Suffix.<br><br>
        7. You are done! Make sure to verify that the configurations are correct by pressing the [Test] button. 
        </h5><br>
        <div class="row"><div class="col-md-2"><a href="../camreg.html" class="btn btn-raised btn-info">Register another camera</a></div></div>
        
        <div class="row"><div class="col-md-2"><a href="../controlpanel.php" class="btn btn-raised btn-info">Proceed to user panel</a></div></div>
        
        
        
        
        </div>
    
    </div>
    

</body>
</html>