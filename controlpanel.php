<?php 
    require("./processing/common.php");
    
    if(!isset($_SESSION['login_user'])){
     header("location: ./loginpage.php");
    }
    
    $akita_user_id = getuserid($db);
    
    $_SESSION['akita_user_id'] = $akita_user_id;
    
    $cameralist = getcameranamelist($db, $akita_user_id);
    $cameralistcount = count($cameralist);
    if(isset($_POST['date'])){
    $dateselected = $_POST['date'];
    }
    
    
    function getuserid($db){
    $useridquery = "SELECT `id_user` FROM `akita_user` WHERE `email` = 'amin@digi-x.my'";
    $useridq =  mysqli_query($db, $useridquery);
    $userid = mysqli_fetch_array($useridq);
    return $userid[0];
    }
    
    function getcameranamelist($db, $akita_user_id){
        $cameralistquery = "SELECT `camera_name`, `id_camera` from `camera` where `akita_user` = ".$akita_user_id;
        $cameralistq = mysqli_query($db, $cameralistquery);
        $cameralist = mysqli_fetch_all($cameralistq);
        return $cameralist;
    }
    
    ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Project Akita</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile support -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Material Design fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap Material Design -->
        <link href="dist/css/bootstrap-material-design.css" rel="stylesheet">
        <link href="dist/css/ripples.min.css" rel="stylesheet">
        <!-- Dropdown.js -->
        <link href="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">
        <!-- Page style -->
        <link href="index.css" rel="stylesheet">
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <div class="bs-component">
            <div class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0)">Project Akita</a>
                    </div>
                    <div class="navbar-collapse collapse navbar-inverse-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="./index.html">Home</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">User Panel</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Support</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="./processing/logout.php">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- THIS IS WHERE THE THING STARTS AFTER THE NAVBAR-->
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <h1>Your camera list</h1>
            </div>
        </div>
        <div class="row">
            <br>
            <br>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                
                
<?php 

    for($x=0; $x<$cameralistcount; $x++){

                echo '<div class="col-md-4">';
                echo '<a href="dateselect.php?cameraid='.$cameralist[$x][1].'">';
                echo '<figure class="figure">';
                echo '<img src="images/CCTVSample.jpg" class="img-rounded" alt="CCTV Sample" width="304" height="236">';
                echo '<figcaption class="figure-caption">'.$cameralist[$x][0].'</figcaption>';
                echo '</figure>';
                echo '</a>';
                echo '</div>';
	}

            ?>

                
                
                
            </div>
            <div class="col-md-1"></div>
        </div>
    </body>
</html>